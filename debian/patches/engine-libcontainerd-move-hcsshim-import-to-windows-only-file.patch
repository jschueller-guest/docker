From: Sebastiaan van Stijn <github@gone.nl>
Date: Mon, 28 Oct 2019 18:30:13 +0100
Subject: [PATCH] libcontainerd: move hcsshim import to windows-only file

This reduces the dependency-graph when building packages for
Linux only.

Signed-off-by: Sebastiaan van Stijn <github@gone.nl>
Origin: upstream, https://github.com/moby/moby/pull/40146/
---
 libcontainerd/remote/client.go         | 41 +++++++++++++++-----------
 libcontainerd/remote/client_linux.go   |  4 +++
 libcontainerd/remote/client_windows.go | 12 +++++++-
 3 files changed, 38 insertions(+), 19 deletions(-)

diff --git a/engine/libcontainerd/remote/client.go b/engine/libcontainerd/remote/client.go
index 094471e0ecd4..93cc4e73b951 100644
--- a/engine/libcontainerd/remote/client.go
+++ b/engine/libcontainerd/remote/client.go
@@ -13,7 +13,6 @@ import (
 	"syscall"
 	"time"
 
-	"github.com/Microsoft/hcsshim/cmd/containerd-shim-runhcs-v1/options"
 	"github.com/containerd/containerd"
 	apievents "github.com/containerd/containerd/api/events"
 	"github.com/containerd/containerd/api/types"
@@ -28,7 +27,6 @@ import (
 	"github.com/docker/docker/errdefs"
 	"github.com/docker/docker/libcontainerd/queue"
 	libcontainerdtypes "github.com/docker/docker/libcontainerd/types"
-
 	"github.com/docker/docker/pkg/ioutils"
 	v1 "github.com/opencontainers/image-spec/specs-go/v1"
 	specs "github.com/opencontainers/runtime-spec/specs-go"
@@ -189,6 +187,27 @@ func (c *client) Start(ctx context.Context, id, checkpointDir string, withStdin
 	}
 	bundle := labels[DockerContainerBundlePath]
 	uid, gid := getSpecUser(spec)
+
+	taskOpts := []containerd.NewTaskOpts{
+		func(_ context.Context, _ *containerd.Client, info *containerd.TaskInfo) error {
+			info.Checkpoint = cp
+			return nil
+		},
+	}
+
+	if runtime.GOOS != "windows" {
+		taskOpts = append(taskOpts, func(_ context.Context, _ *containerd.Client, info *containerd.TaskInfo) error {
+			info.Options = &runctypes.CreateOptions{
+				IoUid:       uint32(uid),
+				IoGid:       uint32(gid),
+				NoPivotRoot: os.Getenv("DOCKER_RAMDISK") != "",
+			}
+			return nil
+		})
+	} else {
+		taskOpts = append(taskOpts, withLogLevel(c.logger.Level))
+	}
+
 	t, err = ctr.NewTask(ctx,
 		func(id string) (cio.IO, error) {
 			fifos := newFIFOSet(bundle, libcontainerdtypes.InitProcessName, withStdin, spec.Process.Terminal)
@@ -196,22 +215,8 @@ func (c *client) Start(ctx context.Context, id, checkpointDir string, withStdin
 			rio, err = c.createIO(fifos, id, libcontainerdtypes.InitProcessName, stdinCloseSync, attachStdio)
 			return rio, err
 		},
-		func(_ context.Context, _ *containerd.Client, info *containerd.TaskInfo) error {
-			info.Checkpoint = cp
-			if runtime.GOOS != "windows" {
-				info.Options = &runctypes.CreateOptions{
-					IoUid:       uint32(uid),
-					IoGid:       uint32(gid),
-					NoPivotRoot: os.Getenv("DOCKER_RAMDISK") != "",
-				}
-			} else {
-				// Make sure we set the runhcs options to debug if we are at debug level.
-				if c.logger.Level == logrus.DebugLevel {
-					info.Options = &options.Options{Debug: true}
-				}
-			}
-			return nil
-		})
+		taskOpts...,
+	)
 	if err != nil {
 		close(stdinCloseSync)
 		if rio != nil {
diff --git a/engine/libcontainerd/remote/client_linux.go b/engine/libcontainerd/remote/client_linux.go
index 2389cbb69d43..486c8538e058 100644
--- a/engine/libcontainerd/remote/client_linux.go
+++ b/engine/libcontainerd/remote/client_linux.go
@@ -94,6 +94,10 @@ func WithBundle(bundleDir string, ociSpec *specs.Spec) containerd.NewContainerOp
 	}
 }
 
+func withLogLevel(_ logrus.Level) containerd.NewTaskOpts {
+	panic("Not implemented")
+}
+
 func newFIFOSet(bundleDir, processID string, withStdin, withTerminal bool) *cio.FIFOSet {
 	config := cio.Config{
 		Terminal: withTerminal,
diff --git a/engine/libcontainerd/remote/client_windows.go b/engine/libcontainerd/remote/client_windows.go
index 35b9a190a8dc..a086d7b3ba6c 100644
--- a/engine/libcontainerd/remote/client_windows.go
+++ b/engine/libcontainerd/remote/client_windows.go
@@ -10,10 +10,10 @@ import (
 	"github.com/containerd/containerd"
 	"github.com/containerd/containerd/cio"
 	"github.com/containerd/containerd/containers"
-
 	libcontainerdtypes "github.com/docker/docker/libcontainerd/types"
 	specs "github.com/opencontainers/runtime-spec/specs-go"
 	"github.com/pkg/errors"
+	"github.com/sirupsen/logrus"
 )
 
 const runtimeName = "io.containerd.runhcs.v1"
@@ -49,6 +49,16 @@ func WithBundle(bundleDir string, ociSpec *specs.Spec) containerd.NewContainerOp
 	}
 }
 
+func withLogLevel(level logrus.Level) containerd.NewTaskOpts {
+	// Make sure we set the runhcs options to debug if we are at debug level.
+	return func(_ context.Context, _ *containerd.Client, info *containerd.TaskInfo) error {
+		if level == logrus.DebugLevel {
+			info.Options = &options.Options{Debug: true}
+		}
+		return nil
+	}
+}
+
 func pipeName(containerID, processID, name string) string {
 	return fmt.Sprintf(`\\.\pipe\containerd-%s-%s-%s`, containerID, processID, name)
 }
